/*Найти лексикографически-минимальную строку, построенную по префикс-функции, в алфавите a-z.
Найти лексикографически-минимальную строку, построенную по z-функции, в алфавите a-z. */
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace std;

const int ALPHABET_SZ = 26;

// нахождение префикс-функции строки
vector<size_t> string_to_pi(const string &str)
{
    vector<size_t> pi(str.size());

    for(size_t i = 1; i < str.size(); ++i)
    {
        size_t cur_pi = pi[i - 1];
        while(cur_pi > 0 && str[cur_pi] != str[i]) {
            cur_pi = pi[cur_pi - 1];
        }
        if(str[cur_pi] == str[i]) {
            ++cur_pi;
        }
        pi[i] = cur_pi;
    }

    return pi;
}

// нахождение z-функции строки
vector<size_t> string_to_z(const string &str)
{
    vector<size_t> z(str.size());

    size_t left, right;
    left = right = 0;
    for(size_t i = 1; i < str.size(); ++i)
    {
        if(i <= right) {
            z[i] = min(z[i - left], right + 1 - i);
        }
        while(i + z[i] < str.size() && str[z[i]] == str[i + z[i]]) {
            ++z[i];
        }
        if(i + z[i] - 1 > right)
        {
            left = i;
            right = i + z[i] - 1;
        }
    }

    return z;
}

// нахождение лексикографически-минимальной строки по префикс-функции
string pi_to_string(const vector<size_t> &pi)
{
    if(pi.empty()) {
        return "";
    }

    string str = "a";
    char new_symbol = 'b';
    for(size_t i = 1; i < pi.size(); ++i)
    {
        if(pi[i] == 0)
        {
            vector<char> prohibited_sym(new_symbol - 'a'); // массив с метками "неиспользования" символов из алфавита
            size_t cur_pi = pi[i - 1];
            prohibited_sym[str[cur_pi] - 'a'] = 1;
            while(cur_pi > 0)
            {
                cur_pi = pi[cur_pi - 1];
                prohibited_sym[str[cur_pi] - 'a'] = 1;
            }

            bool is_new_sym = true;
            for(size_t j = 0; j < prohibited_sym.size(); ++j)
            {
                if(!prohibited_sym[j])
                {
                    str += static_cast<char>(j) + 'a';
                    is_new_sym = false;
                    break;
                }
            }

            if(is_new_sym)
            {
                str += new_symbol;
                ++new_symbol;
            }
        }
        else {
            str += str[pi[i] - 1];
        }
    }

    return str;
}

// нахождение лексикографически-минимальной строки по z-функции
string z_to_string(const vector<size_t> &z)
{
    if(z.empty()) {
        return "";
    }

    string str = "a";
    size_t left, right;
    left = right = 0;
    vector<vector<char>> prohibited_sym(z.size() + 1, vector<char>(ALPHABET_SZ, false)); // массив с метками "неиспользования" символов из алфавита

    for(size_t i = 1; i < z.size(); ++i)
    {
        if(z[i] == 0)
        {
            if(i <= right) {
                str += str[i - left];
            }
            else
            {
                for(size_t j = 1; j < ALPHABET_SZ; ++j)
                {
                    if(!prohibited_sym[i][j])
                    {
                        str += static_cast<char>(j) + 'a';
                        break;
                    }
                }
            }
        }
        else
        {
            str += str[0];
            if(i + z[i] - 1 > right)
            {
                left = i;
                right = i + z[i] - 1;
            }
            prohibited_sym[i + z[i]][str[z[i]] - 'a'] = true;
        }
    }

    return str;
}

//нахождение z-функции по префикс-функции
vector<size_t> pi_to_z(const vector<size_t> &pi)
{
    string str = pi_to_string(pi);
    return string_to_z(str);
}

//нахождение префикс-функции по z-функции
vector<size_t> z_to_pi(const vector<size_t> &z)
{
    string str = z_to_string(z);
    return string_to_pi(str);
}

int main()
{
    ifstream in("input.txt");

    vector<size_t> z;
    size_t x;
    while(in >> x) {
        z.push_back(x);
    }
    in.close();

    string str = z_to_string(z);
    cout << str;

    return 0;
}
