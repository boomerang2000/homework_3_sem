#include <cmath>
#include <iostream>
#include <vector>

using namespace std;

const double PI = 3.14159265358;

struct Vector
{
    double x;
    double y;

    Vector() : x(0), y(0) {}

    Vector(double x, double y) : x(x), y(y) {}

    bool operator>(const Vector &v) const
    {
        return (this->y > v.y) || (this->y == v.y && this->x > v.x);
    }

    Vector operator+(const Vector &v) const
    {
        Vector sum = *this;
        sum.x += v.x;
        sum.y += v.y;
        return sum;
    }

    Vector operator-(const Vector &v) const
    {
        Vector dif = *this;
        dif.x -= v.x;
        dif.y -= v.y;
        return dif;
    }

    Vector operator-() const
    {
        return Vector(0, 0) - *this;
    }
};

double polar_angle(const Vector &v)
{
    if(v.y == 0 && v.x < 0) {
        return -PI;
    }
    return atan2(v.y, v.x);
}

double vector_product(const Vector &v1, const Vector &v2)
{
    return v1.x * v2.y - v1.y * v2.x;
}

vector<Vector> counterclockwise(const vector<Vector> &polygon)
{
    size_t sz = polygon.size();
    size_t max_vector_ind = 0;
    for(size_t i = 1; i < sz; ++i)
    {
        if(polygon[i] > polygon[max_vector_ind]) {
            max_vector_ind = i;
        }
    }

    vector<Vector> new_polygon(sz + 2);
    size_t new_pol_ind = 0;
    for(int i = max_vector_ind; i >= 0; --i) {
        new_polygon[new_pol_ind++] = polygon[i];
    }
    for(size_t i = sz - 1; i > max_vector_ind; --i) {
        new_polygon[new_pol_ind++] = polygon[i];
    }
    new_polygon[sz] = new_polygon[0];
    new_polygon[sz + 1] = new_polygon[1];

    return new_polygon;
}

vector<Vector> find_sum(const vector<Vector> polygon_1,
                        const vector<Vector> polygon_2)
{
    size_t sz_1 = polygon_1.size(), sz_2 = polygon_2.size();
    size_t vert_1 = 0, vert_2 = 0;
    Vector edge_1 = polygon_1[1] - polygon_1[0];
    Vector edge_2 = polygon_2[1] - polygon_2[0];
    vector<Vector> sum;
    while(vert_1 < sz_1 - 2 && vert_2 < sz_2 - 2)
    {
        sum.push_back(polygon_1[vert_1] + polygon_2[vert_2]);
        double angle_1 = polar_angle(edge_1);
        double angle_2 = polar_angle(edge_2);
        if(angle_1 <= angle_2)
        {
            ++vert_1;
            edge_1 = polygon_1[vert_1 + 1] - polygon_1[vert_1];
        }
        if(angle_1 >= angle_2)
        {
            ++vert_2;
            edge_2 = polygon_2[vert_2 + 1] - polygon_2[vert_2];
        }
    }

    sum.push_back(sum[0]);
    return sum;
}

bool is_intersect(const vector<Vector> &polygon_1,
                  const vector<Vector> &polygon_2)
{
    vector<Vector> neg_polygon_2(polygon_2.size());
    for(size_t i = 0; i < polygon_2.size(); ++i) {
        neg_polygon_2[i] = -polygon_2[i];
    }

    vector<Vector> modif_polygon_1 = counterclockwise(polygon_1);
    vector<Vector> modif_polygon_2 = counterclockwise(neg_polygon_2);
    vector<Vector> sum = find_sum(modif_polygon_1, modif_polygon_2);

    bool is_inter = true;
    for(size_t i = 0; i < sum.size() - 1; ++i)
    {
        Vector vector_1 = sum[i + 1] - sum[i];
        Vector vector_2 = -sum[i];
        if(vector_product(vector_1, vector_2) < 0)
        {
            is_inter = false;
            break;
        }
    }

    return is_inter;
}

int main()
{
    size_t n, m;

    cin >> n;
    vector<Vector> first_polygon(n);
    for(size_t i = 0; i < n; ++i) {
        cin >> first_polygon[i].x >> first_polygon[i].y;
    }

    cin >> m;
    vector<Vector> second_polygon(m);
    for(size_t i = 0; i < m; ++i) {
        cin >> second_polygon[i].x >> second_polygon[i].y;
    }

    if(is_intersect(first_polygon, second_polygon)) {
        cout << "YES";
    }
    else {
        cout << "NO";
    }

    return 0;
}
