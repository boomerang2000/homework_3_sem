#include <algorithm>
#include <iostream>
#include <map>
#include <queue>
#include <stack>
#include <string>

using namespace std;

const size_t CELLS_NUM = 64;
const size_t BOARD_LEN = 8;
const int MAX_MOVES = 47;

enum Player
{
    BLACK = 0,
    WHITE = 1
};

struct FigurePosition
{
    int x;
    int y;

    FigurePosition() : x(0), y(0) {}
    FigurePosition(int x, int y) : x(x), y(y) {}
    FigurePosition(const string &str_pos) : x(str_pos[0] - 'a'), y(str_pos[1] - '1') {}
    FigurePosition(const FigurePosition &pos) : x(pos.x), y(pos.y) {}

    bool operator==(const FigurePosition &pos) const
    {
        return x == pos.x && y == pos.y;
    }

    size_t PositionCode() const
    {
        return x * BOARD_LEN + y;
    }
};

const vector<FigurePosition> KW_ATRACKED = {FigurePosition("b2"), FigurePosition("b3"),
                                            FigurePosition("b4"), FigurePosition("c2"),
                                            FigurePosition("c4"), FigurePosition("d2"),
                                            FigurePosition("d3"), FigurePosition("d4")};
const FigurePosition KW_POS("c3");

class BoardPosition
{
public:
    BoardPosition() : level(0), walking_player(WHITE) {}

    BoardPosition(const FigurePosition &qw_pos, const FigurePosition &kb_pos,
                  size_t level, Player wlk_player) : qw_pos(qw_pos), kb_pos(kb_pos),
                  level(level), walking_player(wlk_player) {}

    BoardPosition(const BoardPosition &pos) : qw_pos(pos.qw_pos), kb_pos(pos.kb_pos),
                  level(pos.level), walking_player(pos.walking_player) {}

    vector<BoardPosition> FindNextPoses() const
    {
        vector<BoardPosition> next;
        if(walking_player == BLACK)
        {
            for(int i = -1; i <= 1; ++i)
            {
                if(kb_pos.x + i >= BOARD_LEN || kb_pos.x + i < 0) {
                    continue;
                }
                for(int j = -1; j <= 1; ++j)
                {
                    if((i == 0 && j == 0) || kb_pos.y + j >= BOARD_LEN ||
                       kb_pos.y + j < 0) {
                        continue;
                    }
                    BoardPosition next_pos(qw_pos, FigurePosition (kb_pos.x + i, kb_pos.y + j),
                                           level + 1, WHITE);
                    if(!next_pos.IsAttack()) {
                        next.push_back(next_pos);
                    }
                }
            }
        }
        else
        {
            int i, j, border;
            i = 0;
            border = BOARD_LEN;
            if(qw_pos.y == KW_POS.y)
            {
                if(qw_pos.x >= KW_POS.x) {
                    i = KW_POS.x + 1;
                }
                else {
                    border = KW_POS.x;
                }
            }
            for(; i < border; ++i) // horisontal
            {
                if(i == qw_pos.x) {
                    continue;
                }
                BoardPosition next_pos(FigurePosition (i, qw_pos.y), kb_pos,
                                       level + 1, BLACK);
                if(!next_pos.IsBlackAttack()) {
                    next.push_back(next_pos);
                }
            }

            i = 0;
            border = BOARD_LEN;
            if(qw_pos.x == KW_POS.x)
            {
                if(qw_pos.y >= KW_POS.y) {
                    i = KW_POS.y + 1;
                }
                else {
                    border = KW_POS.y;
                }
            }
            for(; i < border; ++i) // vertical
            {
                if(i == qw_pos.y) {
                    continue;
                }
                BoardPosition next_pos(FigurePosition (qw_pos.x, i), kb_pos,
                                       level + 1, BLACK);
                if(!next_pos.IsBlackAttack()) {
                    next.push_back(next_pos);
                }
            }

            border = BOARD_LEN;
            i = j = 0;
            if(qw_pos.x > qw_pos.y) {
                i = qw_pos.x - qw_pos.y;
            }
            else if(qw_pos.x < qw_pos.y) {
                j = qw_pos.y - qw_pos.x;
            }
            else // one line with kw
            {
                if(qw_pos.x < KW_POS.x) {
                    border = KW_POS.x;
                }
                else {
                    i = j = KW_POS.x + 1;
                }
            }
            for(; i < border && j < border; ++i, ++j) // right diagonal
            {
                if(i == qw_pos.x && j == qw_pos.y) {
                    continue;
                }
                BoardPosition next_pos(FigurePosition (i, j), kb_pos,
                                       level + 1, BLACK);
                if(!next_pos.IsBlackAttack()) {
                    next.push_back(next_pos);
                }
            }

            i = BOARD_LEN - 1;
            j = 0;
            border = BOARD_LEN;
            if(BOARD_LEN - qw_pos.x >= qw_pos.y + 1) {
                i = qw_pos.x + qw_pos.y;
            }
            else {
                j = qw_pos.x + qw_pos.y - BOARD_LEN + 1;
            }
            if(KW_POS.x + KW_POS.y == qw_pos.x + qw_pos.y)
            {
                if(KW_POS.x > qw_pos.x)
                {
                    i = KW_POS.x - 1;
                    j = KW_POS.y + 1;
                }
                else {
                    border = KW_POS.y;
                }
            }
            for(; i >= 0 && j < border; --i, ++j) // left diagonal
            {
                if((i == qw_pos.x && j == qw_pos.y) || (i == KW_POS.x && j == KW_POS.y)) {
                    continue;
                }
                BoardPosition next_pos(FigurePosition (i, j), kb_pos,
                                       level + 1, BLACK);
                if(!next_pos.IsBlackAttack()) {
                    next.push_back(next_pos);
                }
            }
        }

        return next;
    }

    bool IsAttack() const
    {
        bool is_diag_attack = abs(kb_pos.x - qw_pos.x) == abs(kb_pos.y - qw_pos.y);
        bool kw_attack = find(KW_ATRACKED.begin(), KW_ATRACKED.end(), kb_pos) != KW_ATRACKED.end();
        return (kb_pos.x == qw_pos.x) || (kb_pos.y == qw_pos.y) ||
            is_diag_attack || kw_attack;
    }

    size_t PositionCode() const
    {
        return 2 * level * CELLS_NUM * CELLS_NUM + walking_player * CELLS_NUM *
            CELLS_NUM + kb_pos.PositionCode() * CELLS_NUM + qw_pos.PositionCode();
    }

    size_t Level() const
    {
        return level;
    }

    Player CurPlayer() const
    {
        return walking_player;
    }

    bool operator==(const BoardPosition &pos) const
    {
        return kb_pos == pos.kb_pos && qw_pos == pos.qw_pos &&
            walking_player == pos.walking_player && level == pos.level;
    }

    bool operator!=(const BoardPosition &pos) const
    {
        return !(*this == pos);
    }
private:
    bool IsBlackAttack() const
    {
        if(find(KW_ATRACKED.begin(), KW_ATRACKED.end(), qw_pos) != KW_ATRACKED.end()) {
            return false;
        }
        bool is_black_attack = false;
        for(int i = -1; i <= 1; ++i)
        {
            if(kb_pos.x + i >= BOARD_LEN || kb_pos.x + i < 0) {
                continue;
            }
            for(int j = -1; j <= 1; ++j)
            {
                if((i == 0 && j == 0) || kb_pos.y + j >= BOARD_LEN ||
                    kb_pos.y + j < 0) {
                    continue;
                }
                is_black_attack |= (qw_pos.x == kb_pos.x + i &&
                                    qw_pos.y == kb_pos.y + j);
            }
        }
        return is_black_attack;
    }

    FigurePosition qw_pos;
    FigurePosition kb_pos;
    size_t level;
    Player walking_player;
};

class PositionVertex : public BoardPosition
{
public:
    PositionVertex() : BoardPosition(), next(0) {}
    PositionVertex(const BoardPosition &pos) : BoardPosition(pos), next(0) {}
    PositionVertex(const FigurePosition &qw_pos, const FigurePosition &kb_pos,
                   size_t level, Player wlk_player) :
                       BoardPosition(qw_pos, kb_pos, level, wlk_player), next(0) {}
    void AddNext(PositionVertex *pos) { next.push_back(pos); }
    vector<PositionVertex*> GetNextPoses() const { return next; }
private:
    vector<PositionVertex*> next;
};

int find_moves(PositionVertex *cur_pos, vector<int> &moves) //dfs
{
    size_t code = cur_pos->PositionCode();
    if(moves[code] != -1) {
        return moves[code];
    }

    Player cur_player = cur_pos->CurPlayer();
    if(cur_player == WHITE) {
        moves[code] = MAX_MOVES;
    }
    else {
        moves[code] = 0;
    }

    vector<PositionVertex*> next_poses = cur_pos->GetNextPoses();
    if(next_poses.empty()) {
        moves[code] = MAX_MOVES - moves[code];
    }

    for(PositionVertex *next : next_poses)
    {
        size_t next_moves = find_moves(next, moves);
        if(cur_player == WHITE) {
            moves[code] = min(moves[code], static_cast<int>(next_moves + 1));
        }
        else {
            moves[code] = max(moves[code], static_cast<int>(next_moves + 1));
        }
    }

    return moves[code];
}

int moves_to_mate(const string &qw_pos, const string &kb_pos)
{
    FigurePosition qw_start_pos(qw_pos);
    FigurePosition kb_start_pos(kb_pos);
    PositionVertex *start_pos = new PositionVertex(qw_start_pos, kb_start_pos, 0, WHITE);
    PositionVertex *cur_pos = start_pos;

    vector<PositionVertex*> pos_arr(2 * (MAX_MOVES + 1) * CELLS_NUM * CELLS_NUM);
    vector<char> used(2 * (MAX_MOVES + 1) * CELLS_NUM * CELLS_NUM);
    queue<size_t> positions;
    vector<BoardPosition> next_poses = cur_pos->FindNextPoses();

    pos_arr[cur_pos->PositionCode()] = cur_pos;
    used[cur_pos->PositionCode()] = 2;
    for(const BoardPosition &pos : next_poses)
    {
        PositionVertex *new_pos = new PositionVertex(pos);
        pos_arr[new_pos->PositionCode()] = new_pos;
        cur_pos->AddNext(new_pos);
        positions.push(new_pos->PositionCode());
        used[new_pos->PositionCode()] = 1;
    }

    vector<int> moves(2 * (MAX_MOVES + 1) * CELLS_NUM * CELLS_NUM, -1);
    while(!positions.empty())
    {
        size_t pos_code = positions.front();
        positions.pop();
        cur_pos = pos_arr[pos_code];
        used[pos_code] = 2;

        next_poses = cur_pos->FindNextPoses();
        if(next_poses.empty() && cur_pos->IsAttack()) {
            moves[pos_code] = 0;
        }

        for(const BoardPosition &pos : next_poses)
        {
            size_t next_pos_code = pos.PositionCode();
            if(pos.Level() > MAX_MOVES) {
                continue;
            }
            if(!used[next_pos_code])
            {
                PositionVertex *new_pos = new PositionVertex(pos);
                pos_arr[next_pos_code] = new_pos;
                positions.push(next_pos_code);
                used[next_pos_code] = 1;
            }
            cur_pos->AddNext(pos_arr[next_pos_code]);
        }
    }

    int ans = find_moves(start_pos, moves);
    for(PositionVertex *pos : pos_arr) {
        if(pos) {
            delete pos;
        }
    }
    return ans;
}

int main()
{
    string qw_pos, kb_pos;
    cin >> qw_pos >> kb_pos;

    int ans = moves_to_mate(qw_pos, kb_pos);
    if(ans == MAX_MOVES) {
        cout << "IMPOSSIBLE\n";
    }
    else {
        cout << ans;
    }

    return 0;
}
