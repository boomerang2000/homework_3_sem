#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

const double EPS = 0.0000001;

struct Vector
{
    double x;
    double y;
    double z;

    Vector operator+(const Vector &v) const
    {
        Vector sum = *this;
        sum.x += v.x;
        sum.y += v.y;
        sum.z += v.z;
        return sum;
    }

    Vector operator-(const Vector &v) const
    {
        return *this + (-v);
    }

    Vector operator-() const
    {
        Vector neg = *this;
        neg.x = -neg.x;
        neg.y = -neg.y;
        neg.z = -neg.z;
        return neg;
    }

    Vector operator/(double alpha) const
    {
        Vector quotient = *this;
        quotient.x = quotient.x / alpha;
        quotient.y = quotient.y / alpha;
        quotient.z = quotient.z / alpha;
        return quotient;
    }

    double length() const
    {
        return sqrt(x * x + y * y + z * z);
    }
};

Vector vector_product(const Vector &v1, const Vector &v2)
{
    Vector prod;
    prod.x = v1.y * v2.z - v1.z * v2.y;
    prod.y = v1.z * v2.x - v1.x * v2.z;
    prod.z = v1.x * v2.y - v1.y * v2.x;
    return prod;
}

double scalar_product(const Vector &v1, const Vector &v2)
{
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

double min_dist_from_point(const Vector &point, const Vector &seg_point_1,
                           const Vector &seg_point_2)
{
    Vector seg = seg_point_2 - seg_point_1;
    Vector secant_1 = point - seg_point_1;
    Vector secant_2 = point - seg_point_2;

    if(seg.length() == 0) {
        return secant_1.length();
    }
    if(scalar_product(secant_1, seg) < 0) {
        return secant_1.length();
    }
    if(scalar_product(secant_2, -seg) < 0) {
        return secant_2.length();
    }

    double area = vector_product(secant_1, seg).length();
    return area / seg.length();
}

double find_min_distance(const vector<Vector> &points)
{
    Vector point_A = points[0], point_B = points[1];
    while((point_A - point_B).length() > EPS)
    {
        Vector dif = point_B - point_A;
        Vector one_third = point_A + dif / 3;
        Vector two_thirds = point_B - dif / 3;
        double dist_1 = min_dist_from_point(one_third, points[2], points[3]);
        double dist_2 = min_dist_from_point(two_thirds, points[2], points[3]);
        if(dist_1 < dist_2) {
            point_B = two_thirds;
        }
        else {
            point_A = one_third;
        }
    }

    return min_dist_from_point(point_A, points[2], points[3]);
}

int main()
{
    cout.precision(10);
    vector<Vector> points(4);
    for(size_t i = 0; i < 4; ++i) {
        cin >> points[i].x >> points[i].y >> points[i].z;
    }

    double ans = find_min_distance(points);
    cout << ans;

    return 0;
}
