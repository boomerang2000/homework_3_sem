#include <algorithm>
#include <cinttypes>
#include <iostream>
#include <queue>
#include <unordered_map>
#include <vector>

using namespace std;

struct Vector // = point
{
    float x;
    float y;
    float z;

    Vector(float x, float y, float z) : x(x), y(y), z(z) {}

    Vector() : x(0), y(0), z(0) {}

    bool operator<(const Vector &v) const
    {
        return (x < v.x) || (x == v.x && y < v.y) ||
        (x == v.x && y == v.y && z < v.z);
    }

    Vector operator-(const Vector &v) const
    {
        Vector dif = *this;
        dif.x -= v.x;
        dif.y -= v.y;
        dif.z -= v.z;
        return dif;
    }

    float length() const
    {
        return sqrt(x * x + y * y + z * z);
    }
};

struct Face
{
    uint16_t vert_1;
    uint16_t vert_2;
    uint16_t vert_3;

    Face(uint16_t v1, uint16_t v2, uint16_t v3)
    {
        if(v1 < v2 && v1 < v3)
        {
            vert_1 = v1;
            vert_2 = v2;
            vert_3 = v3;
        }
        else if(v2 < v1 && v2 < v3)
        {
            vert_1 = v2;
            vert_2 = v3;
            vert_3 = v1;
        }
        else
        {
            vert_1 = v3;
            vert_2 = v1;
            vert_3 = v2;
        }
    }

    bool operator<(const Face &f) const
    {
        return (vert_1 < f.vert_1) || (vert_1 == f.vert_1 && vert_2 < f.vert_2)
        || (vert_1 == f.vert_1 && vert_2 == f.vert_2 && vert_3 < f.vert_3);
    }
};

struct PairHash
  {
    std::size_t operator()(const pair<size_t, size_t>& p) const
    {
        return hash<size_t>()(p.first)^ hash<size_t>()(p.second);
    }
};

class EdgesUses
{
public:
    char get_uses_num(size_t vert_1, size_t vert_2) const
    {
        pair<size_t, size_t> edge = get_edge(vert_1, vert_2);
        auto it = uses.find(edge);
        if(it == uses.end()) {
            return 0;
        }
        return uses.at(edge);
    }

    void increase_uses_num(size_t vert_1, size_t vert_2)
    {
        pair<size_t, size_t> edge = get_edge(vert_1, vert_2);
        auto it = uses.find(edge);
        if(it == uses.end()) {
            uses[edge] = 1;
        }
        else {
            ++uses[edge];
        }
    }
private:
    pair<size_t, size_t> get_edge(size_t vert_1, size_t vert_2) const
    {
        if(vert_1 > vert_2) {
            swap(vert_1, vert_2);
        }
        return make_pair(vert_1, vert_2);
    }

    unordered_map<pair<size_t, size_t>, char, PairHash> uses;
};

float scalar_product(const Vector &v1, const Vector &v2)
{
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

Vector vector_product(const Vector &v1, const Vector &v2)
{
    Vector prod;
    prod.x = v1.y * v2.z - v1.z * v2.y;
    prod.y = v1.z * v2.x - v1.x * v2.z;
    prod.z = v1.x * v2.y - v1.y * v2.x;
    return prod;
}

float triple_product(const Vector &v1, const Vector &v2, const Vector &v3)
{
    return scalar_product(v1, vector_product(v2, v3));
}

size_t find_right_vertex(const vector<Vector> &points, size_t start_vert,
                         size_t vert_1, size_t vert_2,
                         EdgesUses &used)
{
    size_t sz = points.size();
    size_t right_vert = start_vert;
    Vector edge_1 = points[vert_2] - points[vert_1];
    for(size_t i = 0; i < sz; ++i)
    {
        if(i == start_vert || i == vert_1 || i == vert_2) {
            continue;
        }
        Vector edge_2 = points[i] - points[vert_1];
        Vector edge_3 = points[right_vert] - points[vert_1];
        if(triple_product(edge_1, edge_2, edge_3) < 0 &&
           used.get_uses_num(vert_1, i) < 2) {
            right_vert = i;
        }
    }

    used.increase_uses_num(vert_1, right_vert);
    used.increase_uses_num(vert_2, right_vert);
    return right_vert;
}

size_t find_second_vertex(const vector<Vector> &points, size_t min_index_1)
{
    size_t sz = points.size();
    size_t min_index_2 = 0;
    if(min_index_1 == 0) {
        ++min_index_2;
    }

    Vector basic_x_vector(1, 0, 0);
    Vector edge = points[min_index_2] - points[min_index_1];
    float min_cos = scalar_product(edge, basic_x_vector) / edge.length();
    for(size_t i = 0; i < sz; ++i)
    {
        if(i == min_index_1 || i == min_index_2) {
            continue;
        }

        edge = points[i] - points[min_index_1];
        float i_cos = scalar_product(edge, basic_x_vector) / edge.length();
        if(i_cos < min_cos)
        {
            min_cos = i_cos;
            min_index_2 = i;
        }
    }

    return min_index_2;
}

size_t find_third_vertex(const vector<Vector> &points, size_t min_index_1,
                         size_t min_index_2, EdgesUses &used)
{
    size_t start = 0;
    if(min_index_1 == 0 || min_index_2 == 0)
    {
        ++start;
        if(min_index_1 == 1 || min_index_2 == 1) {
            ++start;
        }
    }

    return find_right_vertex(points, start, min_index_1, min_index_2, used);
}

void find_new_face(const vector<Vector> &points, size_t start_vert,
                   size_t vert_1, size_t vert_2,
                   EdgesUses &used, vector<Face> &faces,
                   queue<Face> &qu)
{
    if(used.get_uses_num(vert_1, vert_2) < 2)
    {
        size_t right_vert = find_right_vertex(points, start_vert, vert_1,
                                              vert_2, used);
        used.increase_uses_num(vert_1, vert_2);
        faces.push_back(Face (vert_1, vert_2, right_vert));
        qu.push(faces.back());
    }
}

vector<Face> min_convex_hull(const vector<Vector> &points)
{
    size_t sz = points.size();
    size_t min_index_1 = 0;
    for(size_t i = 1; i < sz; ++i)
    {
        if(points[i] < points[min_index_1]) {
            min_index_1 = i;
        }
    }

    size_t min_index_2 = find_second_vertex(points, min_index_1);
    EdgesUses used;
    used.increase_uses_num(min_index_1, min_index_2);
    size_t min_index_3 = find_third_vertex(points, min_index_1, min_index_2, used);

    vector<Face> faces;
    queue<Face> qu;
    faces.push_back(Face (min_index_1, min_index_2, min_index_3));

    if(sz == 3) {
        return faces;
    }

    qu.push(faces[0]);
    while(!qu.empty())
    {
        Face face = qu.front();
        qu.pop();
        find_new_face(points, face.vert_1, face.vert_3, face.vert_2, used, faces, qu);
        find_new_face(points, face.vert_2, face.vert_1, face.vert_3, used, faces, qu);
        find_new_face(points, face.vert_3, face.vert_2, face.vert_1, used, faces, qu);
    }

    sort(faces.begin(), faces.end());

    return faces;
}

int main()
{
    size_t m;
    cin >> m;

    for(size_t i = 0; i < m; ++i)
    {
        size_t n;
        cin >> n;

        vector<Vector> points(n);
        for(size_t j = 0; j < n; ++j) {
            cin >> points[j].x >> points[j].y >> points[j].z;
        }

        vector<Face> faces = min_convex_hull(points);
        cout << faces.size() << endl;
        for(Face face : faces)
        {
            cout << "3 " << face.vert_1 << " " << face.vert_2 << " ";
            cout << face.vert_3 << endl;
        }
    }

    return 0;
}