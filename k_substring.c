#include <iostream>
#include <string>
#include <vector>

using namespace std;

const size_t ALPHABET_SZ = 28;

struct Node
{
    vector<Node *> next;
    Node *suf_link;
    size_t common_str_num;
    size_t from;
    size_t to;
    bool is_leaf;
    bool have_sharp;
    bool have_dollar;

    Node(bool is_leaf, size_t from = 0, size_t to = 0) : next(ALPHABET_SZ),
                                                         common_str_num(0),
                                                         from(from), to(to),
                                                         is_leaf(is_leaf) {}

    size_t edge_len() const {
        return to - from;
    }
};

class SuffixTree
{
public:
    SuffixTree(const string &str) : str_sym_nums(str.size())
    {
        size_t sz = str.size();
        for(size_t i = 0; i < sz; ++i) {
            if(str[i] == '#')
            {
                sharp_pos = i;
                str_sym_nums[i] = 26;
            }
            else if(str[i] == '$') {
                str_sym_nums[i] = 27;
            }
            else {
                str_sym_nums[i] = static_cast<size_t>(str[i] - 'a');
            }
        }
        root = new Node(false);

        Node *cur_node = root;
        size_t cur_edge_pos = 0;
        size_t cur_length = 0;
        size_t cur_suf_num = 0;

        for(size_t i = 0; i < sz; ++i)
        {
            size_t cur_sym = str_sym_nums[i];
            Node *prev_node = nullptr;
            while(cur_suf_num <= i)
            {
                if(cur_length == 0) {
                    cur_edge_pos = i;
                }

                Node *next_node = cur_node->next[str_sym_nums[cur_edge_pos]];
                if(!next_node)
                {
                    Node *new_leaf = new Node(true, i, sz);
                    cur_node->next[cur_sym] = new_leaf;
                    create_suf_link(prev_node, cur_node);
                }
                else
                {
                    while(next_node && cur_length >= next_node->edge_len())
                    {
                        cur_length -= next_node->edge_len();
                        cur_edge_pos += next_node->edge_len();
                        cur_node = next_node;
                        next_node = next_node->next[str_sym_nums[cur_edge_pos]];
                    }
                    if(!next_node) {
                        continue;
                    }

                    // next_node != nullptr
                    if(cur_sym == str_sym_nums[next_node->from + cur_length])
                    {
                        create_suf_link(prev_node, cur_node);
                        ++cur_length;
                        break;
                    }

                    Node *new_inner = edge_split(cur_node, cur_length, next_node, i);
                    create_suf_link(prev_node, new_inner);
                }

                if(cur_node != root) {
                    cur_node = cur_node->suf_link ? cur_node->suf_link : root;
                }
                else if(cur_length > 0)
                {
                    --cur_length;
                    cur_edge_pos = cur_suf_num + 1;
                }

                ++cur_suf_num;
            }
        }

        find_common_str_num(root);
    }

    ~SuffixTree()
    {
        delete_node(root);
    }

    void find_k_substr(size_t k, string &str) const
    {
        if(root->common_str_num < k)
        {
            str = "-1";
            return;
        }
        find_k(root, k, str);
    }
private:
    void delete_node(Node *node)
    {
        if(!node) {
            return;
        }
        for(Node *i : node->next) {
            delete_node(i);
        }
        delete node;
    }

    Node *edge_split(Node *cur_node, size_t cur_length, Node *next_node,
                     size_t cur_pos) const
    {
        size_t sz = str_sym_nums.size();
        size_t from = next_node->from;
        Node *new_leaf = new Node(true, cur_pos, sz);
        Node *new_inner = new Node(false, from, from + cur_length);
        next_node->from += cur_length;
        new_inner->next[str_sym_nums[from + cur_length]] = next_node;
        new_inner->next[str_sym_nums[cur_pos]] = new_leaf;
        cur_node->next[str_sym_nums[from]] = new_inner;

        return new_inner;
    }

    void create_suf_link(Node *&prev, Node *cur) const
    {
        if(prev) {
            prev->suf_link = cur;
        }
        prev = cur;
    }

    void find_k(const Node *node, size_t k, string &str) const
    {
        for(size_t i = 0; i <= 25; ++i)
        {
            if(!node->next[i]) {
                continue;
            }
            Node *cur_node = node->next[i];
            if(cur_node->have_sharp && cur_node->have_dollar)
            {
                size_t from = node->next[i]->from;
                size_t len = node->next[i]->edge_len();
                if(k <= len + cur_node->common_str_num)
                {
                    for(size_t j = 0; j < min(k, len); ++j) {
                        str += static_cast<char>(str_sym_nums[from + j] + 'a');
                    }
                    if(k > len) {
                        find_k(cur_node, k - len, str);
                    }
                    break;
                }
                else {
                    k -= len + cur_node->common_str_num;
                }
            }
        }
    }

    void find_common_str_num(Node *node) const
    {
        node->have_sharp = static_cast<bool>(node->next[26]);
        node->have_dollar = static_cast<bool>(node->next[27]);
        for(size_t i = 0; i <= 25; ++i)
        {
            Node *next_node = node->next[i];
            if(next_node)
            {
                if(!next_node->is_leaf)
                {
                    find_common_str_num(next_node);
                    node->have_sharp |= next_node->have_sharp;
                    node->have_dollar |= next_node->have_dollar;
                    if(next_node->have_sharp && next_node->have_dollar)
                    {
                        node->common_str_num += next_node->common_str_num +
                                                next_node->edge_len();
                    }
                }
                else
                {
                    if(next_node->from <= sharp_pos) {
                        node->have_sharp = true;
                    }
                    else {
                        node->have_dollar = true;
                    }
                }
            }
        }
    }

    Node *root;
    vector<size_t> str_sym_nums;
    size_t sharp_pos;
};

string find_k_common_substr(const string &s, const string &t, size_t k)
{
    if(t.size() == 0) {
        return "-1";
    }
    string str_concat = s + "#" + t + "$";
    SuffixTree suf_tree(str_concat);
    string k_substr = "";
    suf_tree.find_k_substr(k, k_substr);
    return k_substr;
}

int main()
{
    string s, t;
    size_t k;
    cin >> s;
    cin.get();
    getline(cin, t);
    cin >> k;

    string k_common_substr = find_k_common_substr(s, t, k);
    cout << k_common_substr;

    return 0;
}
