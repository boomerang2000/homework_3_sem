/*Найдите все вхождения шаблона в строку. Длина шаблона – p, длина строки – n. Время O(n + p), доп. память – O(p).
p <= 30000, n <= 300000.
Использовать один из методов:
- С помощью префикс-функции;
- С помощью z-функции.*/
#include <iostream>
#include <string>
#include <vector>

using namespace std;

// значение префикс-функции на текущем символе
size_t new_pi(const string &pattern, const vector<size_t> &pi, char symbol, size_t cur_pi)
{
    while(cur_pi > 0 && pattern[cur_pi] != symbol) {
        cur_pi = pi[cur_pi - 1];
    }
    if(pattern[cur_pi] == symbol) {
        ++cur_pi;
    }

    return cur_pi;
}

// нахождение префикс-функции шаблона
void find_temp_pi(const string &pattern, vector<size_t> &pi)
{
    pi[0] = 0;

    for(size_t i = 1; i < pattern.size(); ++i) {
        pi[i] = new_pi(pattern, pi, pattern[i], pi[i - 1]);
    }
}

// поиск вхождений шаблона в строку с помощью префикс-функции
vector<size_t> find_entries(const string &pattern)
{
    vector<size_t> indices;
    vector<size_t> pi(pattern.size());
    find_temp_pi(pattern, pi);

    size_t cur_pi = 0;
    char sym = getchar();
    int i = 0;
    while(!isspace(sym = getchar()))
    {
        cur_pi = new_pi(pattern, pi, sym, cur_pi);
        if(cur_pi == pattern.size()) {
            indices.push_back(i + 1 - pattern.size());
        }
        ++i;
    }

    return indices;
}

int main()
{
    string pattern;
    cin >> pattern;

    vector<size_t> indices = find_entries(pattern);

    for(size_t index : indices) {
        cout << index << " ";
    }

    return 0;
}
