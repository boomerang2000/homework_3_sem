#include <iostream>
#include <string>
#include <vector>

using namespace std;

const size_t SHARP_NUM = 26;
const size_t DOLLAR_NUM = 27;
const size_t ALPHABET_SZ = 28;

struct Node
{
    vector<Node *> next;
    Node *suf_link;
    size_t num;
    size_t from;
    size_t to;
    Node *parent;

    Node(size_t from = 0, size_t to = 0, Node *parent = nullptr) :
        next(ALPHABET_SZ), from(from), to(to), parent(parent) {}

    size_t edge_len() const {
        return to - from;
    }
};

class SuffixTree
{
public:
    SuffixTree(const string &str) : str_sym_nums(str.size())
    {
        size_t sz = str.size();
        for(size_t i = 0; i < sz; ++i) {
            if(str[i] == '#') {
                str_sym_nums[i] = SHARP_NUM;
            }
            else if(str[i] == '$')
            {
                dollar_pos = i;
                str_sym_nums[i] = DOLLAR_NUM;
            }
            else {
                str_sym_nums[i] = static_cast<size_t>(str[i] - 'a');
            }
        }
        root = new Node(false);
        tree_size = 1;

        Node *cur_node = root;
        size_t cur_edge_pos = 0;
        size_t cur_length = 0;
        size_t cur_suf_num = 0;

        for(size_t i = 0; i < sz; ++i)
        {
            size_t cur_sym = str_sym_nums[i];
            Node *prev_node = nullptr;
            while(cur_suf_num <= i)
            {
                if(cur_length == 0) {
                    cur_edge_pos = i;
                }

                Node *next_node = cur_node->next[str_sym_nums[cur_edge_pos]];
                if(!next_node)
                {
                    Node *new_leaf = new Node(i, sz, cur_node);
                    ++tree_size;
                    cur_node->next[cur_sym] = new_leaf;
                    create_suf_link(prev_node, cur_node);
                }
                else
                {
                    while(next_node && cur_length >= next_node->edge_len())
                    {
                        cur_length -= next_node->edge_len();
                        cur_edge_pos += next_node->edge_len();
                        cur_node = next_node;
                        next_node = next_node->next[str_sym_nums[cur_edge_pos]];
                    }
                    if(!next_node) {
                        continue;
                    }

                    // next_node != nullptr
                    if(cur_sym == str_sym_nums[next_node->from + cur_length])
                    {
                        create_suf_link(prev_node, cur_node);
                        ++cur_length;
                        break;
                    }

                    Node *new_inner = edge_split(cur_node, cur_length, next_node, i);
                    create_suf_link(prev_node, new_inner);
                }

                if(cur_node != root) {
                    cur_node = cur_node->suf_link ? cur_node->suf_link : root;
                }
                else if(cur_length > 0)
                {
                    --cur_length;
                    cur_edge_pos = cur_suf_num + 1;
                }

                ++cur_suf_num;
            }
        }
    }

    ~SuffixTree()
    {
        delete_node(root);
    }

    size_t size() const
    {
        return tree_size;
    }

    void tree_info() const
    {
        size_t cur_num = 0;
        node_info(root, cur_num);
    }
private:
    void delete_node(Node *node)
    {
        if(!node) {
            return;
        }
        for(Node *i : node->next) {
            delete_node(i);
        }
        delete node;
    }

    Node *edge_split(Node *cur_node, size_t cur_length, Node *next_node,
                     size_t cur_pos)
    {
        size_t sz = str_sym_nums.size();
        size_t from = next_node->from;
        Node *new_inner = new Node(from, from + cur_length, cur_node);
        Node *new_leaf = new Node(cur_pos, sz, new_inner);
        tree_size += 2;
        next_node->from += cur_length;
        next_node->parent = new_inner;
        new_inner->next[str_sym_nums[from + cur_length]] = next_node;
        new_inner->next[str_sym_nums[cur_pos]] = new_leaf;
        cur_node->next[str_sym_nums[from]] = new_inner;

        return new_inner;
    }

    void create_suf_link(Node *&prev, Node *cur) const
    {
        if(prev) {
            prev->suf_link = cur;
        }
        prev = cur;
    }

    void node_info(Node *node, size_t &cur_num) const
    {
        node->num = cur_num++;
        if(node != root)
        {
            printf("%u ", node->parent->num);
            bool w;
            size_t lf, rg;
            if(node->from <= dollar_pos)
            {
                w = 0;
                lf = node->from;
                rg = min(node->to, dollar_pos + 1);
            }
            else
            {
                w = 1;
                lf = node->from - dollar_pos - 1;
                rg = node->to - dollar_pos - 1;
            }
            //I need to use it, otherwise it will be TL
            printf("%d %u %u\n", w, lf, rg);
        }

        if(node->next[SHARP_NUM]) { //sharp
            node_info(node->next[SHARP_NUM], cur_num);
        }
        if(node->next[DOLLAR_NUM]) { //dollar
            node_info(node->next[DOLLAR_NUM], cur_num);
        }
        for(size_t i = 0; i < SHARP_NUM; ++i)
        {
            if(node->next[i]) {
                node_info(node->next[i], cur_num);
            }
        }
    }

    Node *root;
    vector<size_t> str_sym_nums;
    size_t dollar_pos;
    size_t tree_size;
};

void build_two_suf_tree(const string &s, const string &t)
{
    string str_concat = s + t;
    SuffixTree suf_tree(str_concat);
    printf("%u\n", suf_tree.size());
    suf_tree.tree_info();
}

int main()
{
    string s, t;
    cin >> s >> t;

    build_two_suf_tree(s, t);

    return 0;
}
