#include <climits>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

const size_t ALPHABET_SZ = 256;

void zero_phase(const string &str, vector<size_t> &suf_array,
                vector<size_t> &classes)
{
    size_t sz = str.size();
    vector<size_t> counts(ALPHABET_SZ);

    for(size_t i = 0; i < sz; ++i) {
        ++counts[str[i]];
    }

    for(size_t i = 1; i < ALPHABET_SZ; ++i) {
        counts[i] += counts[i - 1];
    }

    for(int i = sz - 1; i >= 0; --i)
    {
        --counts[str[i]];
        suf_array[counts[str[i]]] = i;
    }

    size_t cur_class_num = 0;
    classes[suf_array[0]] = 0;
    for(size_t i = 1; i < sz; ++i)
    {
        if(str[suf_array[i]] != str[suf_array[i - 1]]) {
            ++cur_class_num;
        }
        classes[suf_array[i]] = cur_class_num;
    }
}

void k_phase(vector<size_t> &suf_array, vector<size_t> &classes,
             size_t two_power)
{
    size_t sz = suf_array.size();
    vector<size_t> modif_suf_array(sz);
    for(size_t i = 0; i < sz; ++i) {
        modif_suf_array[i] = (suf_array[i] + sz - two_power) % sz;
    }

    vector<size_t> counts(sz);
    for(size_t i : classes) {
        ++counts[i];
    }

    for(size_t i = 1; i < sz; ++i) {
        counts[i] += counts[i - 1];
    }

    for(int i = sz - 1; i >= 0; --i)
    {
        size_t index = --counts[classes[modif_suf_array[i]]];
        suf_array[index] = modif_suf_array[i];
    }

    size_t cur_class_num = 0;
    vector<size_t> modif_classes(sz);
    modif_classes[suf_array[0]] = 0;

    size_t prev_first_class = classes[suf_array[0]];
    size_t prev_second_class = classes[(suf_array[0] + two_power + sz) % sz];
    for(size_t i = 1; i < sz; ++i)
    {
        size_t cur_first_class = classes[suf_array[i]];
        size_t cur_second_class = classes[(suf_array[i] + two_power + sz) % sz];
        if(cur_first_class != prev_first_class ||
           cur_second_class != prev_second_class) {
            ++cur_class_num;
        }
        modif_classes[suf_array[i]] = cur_class_num;
        prev_first_class = cur_first_class;
        prev_second_class = cur_second_class;
    }

    classes = move(modif_classes);
}

vector<size_t> find_suf_array(const string &str)
{
    size_t sz = str.size();
    vector<size_t> suf_array(sz);
    vector<size_t> classes(sz);

    zero_phase(str, suf_array, classes);

    size_t two_power = 1;
    while(two_power < sz)
    {
        k_phase(suf_array, classes, two_power);
        two_power <<= 1;
    }

    return suf_array;
}

//Kasai algorithm
vector<size_t> find_lcp(const string &str, const vector<size_t> &suf_array)
{
    size_t sz = str.size();
    vector<size_t> suf_order(sz);
    for(size_t i = 0; i < sz; ++i) {
        suf_order[suf_array[i]] = i;
    }

    vector<size_t> lcp(sz);
    size_t cur_lcp = 0;
    for(size_t i = 0; i < sz; ++i)
    {
        if(cur_lcp > 0) {
            --cur_lcp;
        }

        if(suf_order[i] + 1 == sz) {
            continue;
        }

        size_t next_suf = suf_array[suf_order[i] + 1];
        while(i + cur_lcp < sz && next_suf + cur_lcp < sz &&
              str[i + cur_lcp] == str[next_suf + cur_lcp]) {
            ++cur_lcp;
        }

        lcp[suf_order[i]] = cur_lcp;
    }
    lcp[sz - 1] = 0;

    return lcp;
}

size_t dif_substrings_num(string &str)
{
    str += "$";
    size_t sz = str.size();
    vector<size_t> suf_array = find_suf_array(str);
    vector<size_t> lcp = find_lcp(str, suf_array);

    size_t dif_str_num = (sz - 1) * (sz - 1);
    for(size_t i = 1; i < sz; ++i) {
        dif_str_num -= suf_array[i] + lcp[i];
    }

    return dif_str_num;
}

int main()
{
    string str;
    cin >> str;

    size_t ans = dif_substrings_num(str);
    cout << ans;

    return 0;
}
