#ifndef BIGINTEGER_H_INCLUDED
#define BIGINTEGER_H_INCLUDED
#include <iostream>
#include <string>
#include <vector>

const size_t BASE = 1e+9;
const size_t BASIC_MUL_NUM = 8;

using namespace std;

class BigInteger
{
public:
    BigInteger();
    BigInteger(int val);
    BigInteger(vector<size_t>::iterator start, vector<size_t>::iterator finish);
    explicit operator bool() const;
    string toString() const;
    friend ostream &operator<<(ostream &out, const BigInteger &big_int);
    friend istream &operator>>(istream &in, BigInteger &big_int);
    bool operator<(const BigInteger &big_int) const;
    bool operator==(const BigInteger &big_int) const;
    bool operator!=(const BigInteger &big_int) const;
    bool operator<=(const BigInteger &big_int) const;
    bool operator>=(const BigInteger &big_int) const;
    bool operator>(const BigInteger &big_int) const;
    BigInteger operator-() const;
    BigInteger &operator+=(const BigInteger &big_int);
    BigInteger &operator-=(const BigInteger &big_int);
    BigInteger &operator*=(const BigInteger &big_int);
    BigInteger &operator/=(const BigInteger &big_int);
    BigInteger &operator%=(const BigInteger &big_int);
    BigInteger &operator++();
    BigInteger &operator--();
    BigInteger operator++(int);
    BigInteger operator--(int);
    friend BigInteger RecursiveMult(BigInteger &big_int_1, BigInteger &big_int_2);
    friend BigInteger BasicMult(BigInteger &big_int_1, BigInteger &big_int_2);
private:
    int FirstNotEqBlock(const BigInteger &big_int) const;
    void ClearNils();
    void Clear();
    BigInteger Abs() const;

    vector<size_t> blocks;
    bool is_neg;
};

BigInteger operator+(const BigInteger &big_int_1, const BigInteger &big_int_2);
BigInteger operator-(const BigInteger &big_int_1, const BigInteger &big_int_2);
BigInteger operator*(const BigInteger &big_int_1, const BigInteger &big_int_2);
BigInteger operator/(const BigInteger &big_int_1, const BigInteger &big_int_2);
BigInteger operator%(const BigInteger &big_int_1, const BigInteger &big_int_2);

size_t FindQuotient(const BigInteger &cur_div, const BigInteger &big_int)
{
    size_t left = 0, right = BASE;
    while(left < right - 1)
    {
        size_t mid = (left + right) / 2;
        BigInteger quot = big_int * mid;
        if(quot <= cur_div) {
            left = mid;
        }
        else {
            right = mid;
        }
    }
    if(big_int * right <= cur_div) {
        return right;
    }
    else {
        return left;
    }
}

BigInteger BasicMult(BigInteger &big_int_1, BigInteger &big_int_2)
{
    size_t sz = big_int_1.blocks.size();
    vector<long long> mult(2 * sz);
    for(size_t i = 0; i < sz; ++i)
    {
        for(size_t j = 0; j < sz; ++j) {
            mult[i + j] += 1ll * big_int_1.blocks[i] * big_int_2.blocks[j];
        }
    }

    BigInteger prod;
    prod.blocks.clear();
    size_t carry = 0;
    for(size_t i = 0; i < sz * 2; ++i)
    {
        mult[i] += carry;
        prod.blocks.push_back(mult[i] % BASE);
        carry = mult[i] / BASE;
    }
    prod.ClearNils();
    return prod;
}

BigInteger RecursiveMult(BigInteger &big_int_1, BigInteger &big_int_2)
{
    vector<size_t> &blocks_1 = big_int_1.blocks;
    vector<size_t> &blocks_2 = big_int_2.blocks;
    if(blocks_1.size() < blocks_2.size()) {
        blocks_1.resize(blocks_2.size());
    }
    else if(blocks_1.size() > blocks_2.size()) {
        blocks_2.resize(blocks_1.size());
    }

    size_t sz = blocks_1.size();
    if(sz < BASIC_MUL_NUM) {
        return BasicMult(big_int_1, big_int_2);
    }

    BigInteger left_1(blocks_1.begin(), blocks_1.begin() + sz / 2);
    BigInteger right_1(blocks_1.begin() + sz / 2, blocks_1.end());
    BigInteger left_2(blocks_2.begin(), blocks_2.begin() + sz / 2);
    BigInteger right_2(blocks_2.begin() + sz / 2, blocks_2.end());
    BigInteger sum_1 = left_1 + right_1;
    BigInteger sum_2 = left_2 + right_2;

    BigInteger prod_1 = RecursiveMult(sum_1, sum_2);
    BigInteger prod_2 = RecursiveMult(left_1, left_2);
    BigInteger prod_3 = RecursiveMult(right_1, right_2);

    prod_1 -= prod_2 + prod_3;
    prod_1.blocks.insert(prod_1.blocks.begin(), sz / 2, 0);
    prod_3.blocks.insert(prod_3.blocks.begin(), (sz / 2) * 2, 0);

    BigInteger prod = prod_1 + prod_2 + prod_3;
    prod.ClearNils();
    return prod;
}

BigInteger::BigInteger() : blocks(1), is_neg(0)
{
    blocks[0] = 0;
}

BigInteger::BigInteger(int val) : blocks(0)
{
    if(val >= 0) {
        is_neg = false;
    }
    else
    {
        val = -val;
        is_neg = true;
    }
    while(val)
    {
        blocks.push_back(val % BASE);
        val /= BASE;
    }

    if(blocks.empty()) {
        blocks.push_back(0);
    }
}

BigInteger::BigInteger(vector<size_t>::iterator start, vector<size_t>::iterator finish)
{
    is_neg = false;
    for(auto it = start; it != finish; ++it) {
        blocks.push_back(*it);
    }
}

BigInteger::operator bool() const
{
    return blocks.back() != 0;
}

string BigInteger::toString() const
{
    if(blocks.back() == 0) {
        return "0";
    }
    size_t sz = blocks.size();
    string str;
    if(is_neg) {
        str += "-";
    }
    for(int i = sz - 1; i >= 0; --i)
    {
        size_t cur_block = blocks[i];
        size_t cur_deg = BASE / 10;
        bool first_notnil_flag = false;
        while(cur_deg)
        {
            size_t digit = cur_block / cur_deg;
            first_notnil_flag |= digit;
            if(first_notnil_flag || i != static_cast<int>(sz - 1)) {
                str += static_cast<char>(digit + '0');
            }
            cur_block -= digit * cur_deg;
            cur_deg /= 10;
        }
    }
    return str;
}

ostream &operator<<(ostream &out, const BigInteger &big_int)
{
    out << big_int.toString();
    return out;
}

istream &operator>>(istream &in, BigInteger &big_int)
{
    string str;
    in >> str;
    size_t sz = str.size();
    int first_dig_num = 0;
    big_int.Clear();
    if(str[0] == '-')
    {
        big_int.is_neg = true;
        first_dig_num = 1;
    }

    size_t cur_deg = 1;
    size_t cur_block = 0;
    for(int i = sz - 1; i >= first_dig_num; --i)
    {
        cur_block += static_cast<char>(str[i] - '0') * cur_deg;
        cur_deg *= 10;
        if(cur_deg >= BASE)
        {
            big_int.blocks.push_back(cur_block);
            cur_deg = 1;
            cur_block = 0;
        }
    }
    if(cur_block || big_int.blocks.empty()) {
        big_int.blocks.push_back(cur_block);
    }
    return in;
}

bool BigInteger::operator<(const BigInteger &big_int) const
{
    if(blocks.back() == 0) {
        return false;
    }
    int not_equal = false;
    if(blocks.size() == big_int.blocks.size()) {
        not_equal = FirstNotEqBlock(big_int);
    }
    if(is_neg && big_int.is_neg) {
        return (blocks.size() > big_int.blocks.size()) ||
            (blocks.size() == big_int.blocks.size() &&
             not_equal != -1 &&
             blocks[not_equal] > big_int.blocks[not_equal]);
    }
    if(!is_neg && !big_int.is_neg)
    {
        return (blocks.size() < big_int.blocks.size()) ||
            (blocks.size() == big_int.blocks.size() &&
             not_equal != -1 &&
             blocks[not_equal] < big_int.blocks[not_equal]);
    }
    return is_neg;
}

bool BigInteger::operator==(const BigInteger &big_int) const
{
    if(blocks.back() == 0) {
        return true;
    }
    int not_equal = false;
    if(blocks.size() == big_int.blocks.size()) {
        not_equal = FirstNotEqBlock(big_int);
    }
    return (blocks.size() == big_int.blocks.size()) &&
        (is_neg == big_int.is_neg) && (not_equal == -1);
}

bool BigInteger::operator!=(const BigInteger &big_int) const
{
    return !(*this == big_int);
}

bool BigInteger::operator<=(const BigInteger &big_int) const
{
    return (*this < big_int) || (*this == big_int);
}

bool BigInteger::operator>=(const BigInteger &big_int) const
{
    return !(*this < big_int);
}

bool BigInteger::operator>(const BigInteger &big_int) const
{
    return !(*this <= big_int);
}

BigInteger BigInteger::operator-() const
{
    BigInteger neg = *this;
    neg.is_neg = !neg.is_neg;
    return neg;
}

BigInteger &BigInteger::operator+=(const BigInteger &big_int)
{
    if(is_neg)
    {
        is_neg = false;
        if(big_int.is_neg) {
            *this += -big_int;
        }
        else {
            *this -= big_int;
        }
        return *this = -(*this);
    }

    if(big_int.is_neg) {
        return (*this -= -big_int);
    }

    size_t sz_1 = blocks.size();
    size_t sz_2 = big_int.blocks.size();
    size_t cur_dig = 0;
    size_t carry = 0;
    while(cur_dig < max(sz_1, sz_2) || carry)
    {
        if(cur_dig >= sz_1) {
            blocks.push_back(0);
        }
        size_t term = cur_dig < sz_2 ? big_int.blocks[cur_dig] : 0;
        blocks[cur_dig] += term + carry;
        if(blocks[cur_dig] >= BASE)
        {
            blocks[cur_dig] -= BASE;
            carry = 1;
        }
        else {
            carry = 0;
        }
        ++cur_dig;
    }

    return *this;
}

BigInteger &BigInteger::operator-=(const BigInteger &big_int)
{
    if(is_neg)
    {
        *this = -*this;
        if(big_int.is_neg) {
            *this -= -big_int;
        }
        else {
            *this += big_int;
        }
        return *this = -(*this);
    }

    if(big_int.is_neg) {
        return (*this += -big_int);
    }

    if(*this < big_int)
    {
        BigInteger tmp_int = big_int;
        tmp_int -= *this;
        *this = move(-tmp_int);
        return *this;
    }

    size_t carry = 0;
    for(size_t i = 0; i < big_int.blocks.size() || carry; ++i)
    {
        size_t temp = i < big_int.blocks.size() ? big_int.blocks[i] : 0;
        temp += carry;
        if(blocks[i] < temp)
        {
            blocks[i] += BASE;
            carry = 1;
        }
        else {
            carry = 0;
        }
        blocks[i] -= temp;
    }

    ClearNils();
    return *this;
}

BigInteger &BigInteger::operator*=(const BigInteger &big_int)
{
    BigInteger abs_1 = Abs();
    BigInteger abs_2 = big_int.Abs();
    bool neg = is_neg;
    *this = RecursiveMult(abs_1, abs_2);
    is_neg = neg ^ big_int.is_neg;
    return *this;
}

BigInteger &BigInteger::operator/=(const BigInteger &big_int)
{
    if(this->Abs() < big_int.Abs()) {
        return *this = BigInteger(0);
    }
    is_neg ^= big_int.is_neg;
    vector<size_t> src_blocks = blocks;
    blocks.clear();
    BigInteger cur_div;
    cur_div.blocks.clear();
    size_t sz = src_blocks.size();
    for(int i = sz - 1; i >= 0; --i)
    {
        cur_div.blocks.insert(cur_div.blocks.begin(), src_blocks[i]);
        size_t loc_quot = FindQuotient(cur_div, big_int.Abs());
        cur_div -= big_int.Abs() * loc_quot;
        cur_div.ClearNils();
        if(!cur_div.blocks.back()) {
            cur_div.blocks.pop_back();
        }
        blocks.insert(blocks.begin(), loc_quot);
    }
    ClearNils();
    return *this;
}

BigInteger &BigInteger::operator%=(const BigInteger &big_int)
{
    return *this -= (*this / big_int) * big_int;
}

BigInteger operator+(const BigInteger &big_int_1, const BigInteger &big_int_2)
{
    BigInteger sum = big_int_1;
    sum += big_int_2;
    return sum;
}

BigInteger operator-(const BigInteger &big_int_1, const BigInteger &big_int_2)
{
    BigInteger dif = big_int_1;
    dif -= big_int_2;
    return dif;
}

BigInteger operator*(const BigInteger &big_int_1, const BigInteger &big_int_2)
{
    BigInteger prod = big_int_1;
    prod *= big_int_2;
    return prod;
}

BigInteger operator/(const BigInteger &big_int_1, const BigInteger &big_int_2)
{
    BigInteger quot = big_int_1;
    quot /= big_int_2;
    return quot;
}

BigInteger operator%(const BigInteger &big_int_1, const BigInteger &big_int_2)
{
    BigInteger residue = big_int_1;
    residue %= big_int_2;
    return residue;
}

BigInteger &BigInteger::operator++()
{
    *this += 1;
    return *this;
}

BigInteger &BigInteger::operator--()
{
    *this -= 1;
    return *this;
}

BigInteger BigInteger::operator++(int)
{
    BigInteger old_int = *this;
    ++(*this);
    return old_int;
}

BigInteger BigInteger::operator--(int)
{
    BigInteger old_int = *this;
    --(*this);
    return old_int;
}

int BigInteger::FirstNotEqBlock(const BigInteger &big_int) const
{
    int i = blocks.size() - 1;
    while(i >= 0 && blocks[i] == big_int.blocks[i]) {
        --i;
    }
    return i;
}

void BigInteger::ClearNils()
{
    while(blocks.back() == 0 && blocks.size() > 1) {
        blocks.pop_back();
    }
}

void BigInteger::Clear()
{
    blocks.clear();
    is_neg = false;
}

BigInteger BigInteger::Abs() const
{
    return is_neg ? -(*this) : *this;
}

#endif // BIGINTEGER_H_INCLUDED
