#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <limits>
#include <queue>
#include <stack>
#include <unordered_map>
#include <vector>

using namespace std;

struct Point2D
{
    double x;
    double y;

    Point2D() : x(0), y(0) {}

    Point2D(double x, double y) : x(x), y(y) {}

    bool operator<(const Point2D &p) const
    {
        return (x < p.x) || (x == p.x && y < p.y);
    }

    Point2D operator-(const Point2D &p) const
    {
        Point2D dif = *this;
        dif.x -= p.x;
        dif.y -= p.y;
        return dif;
    }
};

struct Point3D {
    double x;
    double y;
    double z;
    size_t num;

    Point3D() : x(0), y(0), z(0), num(0) {}

    Point3D(double x, double y, double z, size_t num) : x(x), y(y), z(z), num(num) {}

    bool operator<(const Point3D &p) const
    {
        return (x < p.x) || (x == p.x && y < p.y);
    }

    Point3D operator-(const Point3D &p) const
    {
        Point3D dif = *this;
        dif.x -= p.x;
        dif.y -= p.y;
        dif.z -= p.z;
        return dif;
    }
};

struct HullPoint : Point3D {
    HullPoint *prev;
    HullPoint *next;

    HullPoint(const Point3D &point, HullPoint *prev, HullPoint *next) :
                Point3D(point), prev(prev), next(next) {}
};

struct HullEvent {
    double time;
    HullPoint *point;

    HullEvent(double time, HullPoint *point) :
        time(time), point(point) {}

    void change_hull() const
    {
        HullPoint *next_p;
        HullPoint *prev_p;
        if(!point->prev || point->prev->next != point) {

            next_p = prev_p = point;
        }
        else
        {
            next_p = point->next;
            prev_p = point->prev;
        }

        if(point->next) {
            point->next->prev = prev_p;
        }
        if(point->prev) {
            point->prev->next = next_p;
        }
    }
};

const double infinity = numeric_limits<double>::infinity();

double vector_product(const Point2D &point_1, const Point2D &point_2)
{
    return point_1.x * point_2.y - point_1.y * point_2.x;
}

double xy_vector_product(const HullPoint &point_1, const HullPoint &point_2,
                         const HullPoint &point_3)
{
    Point3D vect_1 = static_cast<Point3D>(point_2) - static_cast<Point3D>(point_1);
    Point3D vect_2 = static_cast<Point3D>(point_3) - static_cast<Point3D>(point_1);
    return vect_1.x * vect_2.y - vect_1.y * vect_2.x;
}

double turn_change_time(const HullPoint &point_1, const HullPoint &point_2,
                        const HullPoint &point_3)
{
    Point3D vect_1 = static_cast<Point3D>(point_2) - static_cast<Point3D>(point_1);
    Point3D vect_2 = static_cast<Point3D>(point_3) - static_cast<Point3D>(point_1);
    double change_time = (vect_1.x * vect_2.z - vect_1.z * vect_2.x) /
                            (vect_1.x * vect_2.y - vect_1.y * vect_2.x);
    return change_time;
}

void find_init_verts(vector<HullPoint> &hull, size_t mid, HullPoint *&left_vert,
                     HullPoint *&right_vert)
{
    left_vert = &hull[mid];
    right_vert = &hull[mid + 1];
    double prod_1 = !right_vert->next ? 1 : xy_vector_product(*left_vert, *right_vert, *(right_vert->next));
    double prod_2 = !left_vert->prev ? 1 : xy_vector_product(*(left_vert->prev), *left_vert, *right_vert);
    while(prod_1 < 0 || prod_2 < 0)
    {
        if(prod_1 < 0) {
            right_vert = right_vert->next;
        }
        else if(prod_2 < 0) {
            left_vert = left_vert->prev;
        }
        prod_1 = !right_vert->next ? 1 : xy_vector_product(*left_vert, *right_vert, *(right_vert->next));
        prod_2 = !left_vert->prev ? 1 : xy_vector_product(*(left_vert->prev), *left_vert, *right_vert);
    }
}

vector<HullEvent> find_union_events(const vector<HullEvent> &left_events,
                                    const vector<HullEvent> &right_events,
                                    HullPoint *&left_vert, HullPoint *&right_vert)
{
    vector<HullEvent> events;
    size_t left_event_num = 0;
    size_t right_event_num = 0;
    double cur_time = -infinity;
    vector<double> event_time(6);
    vector<bool> is_correct(6);
    while(cur_time != infinity)
    {
        is_correct[0] = left_event_num < left_events.size();
        is_correct[1] = right_event_num < right_events.size();
        is_correct[2] = left_vert->next;
        is_correct[3] = right_vert->prev;
        is_correct[4] = left_vert->prev;
        is_correct[5] = right_vert->next;

        if(is_correct[0]) {
            event_time[0] = left_events[left_event_num].time;
        }
        if(is_correct[1]) {
            event_time[1] = right_events[right_event_num].time;
        }
        if(is_correct[2]) {
            event_time[2] = turn_change_time(*left_vert, *(left_vert->next),
                                             *right_vert);
        }
        if(is_correct[3]) {
            event_time[3] = turn_change_time(*left_vert, *(right_vert->prev),
                                             *right_vert);
        }
        if(is_correct[4]) {
            event_time[4] = turn_change_time(*(left_vert->prev), *left_vert,
                                             *right_vert);
        }
        if(is_correct[5]) {
            event_time[5] = turn_change_time(*left_vert, *right_vert,
                                             *(right_vert->next));
        }

        double min_time = infinity;
        int first_event = -1;
        for(size_t i = 0; i < 6; ++i)
        {
            if(is_correct[i] && event_time[i] > cur_time && event_time[i] < min_time)
            {
                min_time = event_time[i];
                first_event = i;
            }
        }
        if(first_event < 0) {
            break;
        }
        cur_time = event_time[first_event];

        if(first_event == 0)
        {
            if(left_events[left_event_num].point->x < left_vert->x) {
                events.push_back(left_events[left_event_num]);
            }
            left_events[left_event_num].change_hull();
            ++left_event_num;
        }
        else if(first_event == 1)
        {
            if(right_events[right_event_num].point->x > right_vert->x) {
                events.push_back(right_events[right_event_num]);
            }
            right_events[right_event_num].change_hull();
            ++right_event_num;
        }
        else if(first_event == 2)
        {
            left_vert = left_vert->next;
            events.push_back(HullEvent(cur_time, left_vert));
        }
        else if(first_event == 3)
        {
            right_vert = right_vert->prev;
            events.push_back(HullEvent(cur_time, right_vert));
        }
        else if(first_event == 4)
        {
            events.push_back(HullEvent(cur_time, left_vert));
            left_vert = left_vert->prev;
        }
        else
        {
            events.push_back(HullEvent(cur_time, right_vert));
            right_vert = right_vert->next;
        }
    }
    return events;
}

vector<HullEvent> find_events(vector<HullPoint> &hull, size_t left,
                                        size_t right)
{
    vector<HullEvent> events;
    if(left == right) {
        return events;
    }

    size_t mid = (left + right) / 2;
    vector<HullEvent> left_events = find_events(hull, left, mid);
    vector<HullEvent> right_events = find_events(hull, mid + 1, right);

    HullPoint *left_vert, *right_vert;
    find_init_verts(hull, mid, left_vert, right_vert);
    events = find_union_events(left_events, right_events, left_vert, right_vert);

    left_vert->next = right_vert;
    right_vert->prev = left_vert;
    for(int i = events.size() - 1; i >= 0; --i)
    {
        HullEvent &event = events[i];
        if(event.point->x <= left_vert->x || event.point->x >= right_vert->x)
        {
            if(event.point == left_vert) {
                left_vert = left_vert->prev;
            }
            else if(event.point == right_vert) {
                right_vert = right_vert->next;
            }
            event.change_hull();
        }
        else
        {
            left_vert->next = right_vert->prev = event.point;
            event.point->prev = left_vert;
            event.point->next = right_vert;
            if(event.point->x < hull[mid + 1].x) {
                left_vert = event.point;
            }
            else {
                right_vert = event.point;
            }
        }
    }

    return events;
}

void change_degrees(const vector<Point3D> &points, vector<size_t> &deg)
{
    vector<HullPoint> hull;
    for(const Point3D &point : points) {
        hull.push_back(HullPoint(point, nullptr, nullptr));
    }
    vector<HullEvent> events = find_events(hull, 0, hull.size() - 1);
    for(HullEvent &event : events)
    {
        ++deg[event.point->num];
        ++deg[event.point->next->num];
        ++deg[event.point->prev->num];
        event.change_hull();
    }
}

vector<size_t> find_vert_degrees(vector<Point3D> &points)
{
    sort(points.begin(), points.end());
    vector<size_t> deg(points.size());
    change_degrees(points, deg);
    for(Point3D &point : points) {
        point.z = -point.z;
    }
    change_degrees(points, deg);

    return deg;
}

void sort_by_polar(vector<Point2D> &points, size_t min_point)
{
    size_t sz = points.size();
    swap(points[0], points[min_point]);
    vector<pair<double, size_t>> angles(sz - 1);
    for(size_t i = 1; i < sz; ++i)
    {
        Point2D vec = points[i] - points[0];
        angles[i - 1] = make_pair(atan2(vec.y, vec.x), i);
    }
    sort(angles.begin(), angles.end());
    vector<Point2D> sorted_points(sz);
    sorted_points[0] = points[0];
    for(size_t i = 1; i < sz; ++i) {
        sorted_points[i] = points[angles[i - 1].second];
    }
    points = move(sorted_points);
}

vector<bool> find_verts_in_2d_hull(vector<Point2D> &points)
{
    size_t sz = points.size();
    size_t min_point = 0;
    for(size_t i = 1; i < sz; ++i)
    {
        if(points[i] < points[min_point]) {
           min_point = i;
        }
    }

    sort_by_polar(points, min_point);
    stack<size_t> points_stack;
    points_stack.push(0);
    points_stack.push(1);
    for(size_t i = 2; i < sz; ++i)
    {
        size_t top_vert = points_stack.top();
        points_stack.pop();
        while(vector_product(points[i] - points[top_vert],
                             points[points_stack.top()] - points[top_vert]) <= 0)
        {
            top_vert = points_stack.top();
            points_stack.pop();
        }
        points_stack.push(top_vert);
        points_stack.push(i);
    }

    vector<bool> is_in_2d_hull(sz, false);
    while(!points_stack.empty())
    {
        is_in_2d_hull[points_stack.top()] = true;
        points_stack.pop();
    }
    return is_in_2d_hull;
}

double average_sides_num(vector<Point2D> &points)
{
    size_t sz = points.size();
    if(sz <= 3) {
        return 0;
    }
    double deg_sum = 0;
    size_t hull_verts_num = 0;
    vector<bool> is_in_2d_hull = find_verts_in_2d_hull(points);

    vector<Point3D> points_3d(sz);
    size_t i = 0;
    for(size_t j = 0; j < sz; ++j)
    {
        //I need to change the x, since the Chan's algorithm doesn't work at points with the same x
        double x = points[j].x + 0.0000001 * i++;
        points_3d[j].x = x;
        points_3d[j].y = points[j].y;
        points_3d[j].z = x * x + points[j].y * points[j].y;
        points_3d[j].num = j;
    }

    vector<size_t> deg = find_vert_degrees(points_3d);
    for(size_t i = 0; i < sz; ++i) {
        if(is_in_2d_hull[i]) {
            ++hull_verts_num;
        }
        else {
            deg_sum += deg[i];
        }
    }

    return (sz - hull_verts_num) ? deg_sum / (sz - hull_verts_num) : 0;
}

int main()
{
    cout.precision(6);
    ifstream in("input.txt");
    double x, y;
    vector<Point2D> points;
    in >> x;
    while(!in.eof())
    {
        in >> y;
        points.push_back(Point2D(x, y));
        in >> x;
    }
    in.close();

    double ans = average_sides_num(points);
    cout << std::fixed << ans;

    return 0;
}
