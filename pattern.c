/*Шаблон поиска задан строкой длины m, в которой кроме обычных символов могут встречаться символы “?”. Найти позиции всех
вхождений шаблона в тексте длины n. Каждое вхождение шаблона предполагает, что все обычные символы совпадают с соответствующими из текста,
а вместо символа “?” в тексте встречается произвольный символ. Время работы - O(n + m + Z), где Z - общее -число вхождений подстрок шаблона
“между вопросиками” в исходном тексте. */
#include <iostream>
#include <string>
#include <vector>

using namespace std;

const size_t ALPHABET_SZ = 256;

struct Node {
    vector<Node*> next;
    vector<Node*> go;
    Node *prev;
    Node *suf_link;
    Node *compressed_link;
    char prev_sym;
    vector<int> str_num;

    Node(Node *prev = nullptr,
         char prev_sym = '\0') : next(ALPHABET_SZ), go(ALPHABET_SZ), prev(prev),
                                suf_link(nullptr), compressed_link(nullptr),
                                prev_sym(prev_sym) {}
};

class Trie {
public:
    Trie(const vector<string> &strs)
    {
        root = new Node();
        for(size_t i = 0; i < strs.size(); ++i) {
            add_string(strs[i], i);
        }
    }

    Node *get_root()
    {
        return root;
    }

    Node *get_suf_link(Node *node)
    {
        if(node->suf_link) {
            return node->suf_link;
        }

        if(node == root || node->prev == root) {
            node->suf_link = root;
        }
        else {
            node->suf_link = get_next_node(get_suf_link(node->prev),
                                           node->prev_sym);
        }

        return node->suf_link;
    }

    Node *get_next_node(Node *node, char sym)
    {
        if(node->go[sym]) {
            return node->go[sym];
        }

        if(node->next[sym]) {
            node->go[sym] = node->next[sym];
        }
        else if(node == root) {
            node->go[sym] = root;
        }
        else {
            node->go[sym] = get_next_node(get_suf_link(node), sym);
        }
        return node->go[sym];
    }

    Node *get_compressed_link(Node *node)
    {
        if(node->compressed_link) {
            return node->compressed_link;
        }

        Node *suf_link = get_suf_link(node);
        if(suf_link == root) {
            node->compressed_link = root;
        }
        else if(!suf_link->str_num.empty()) {
            node->compressed_link = suf_link;
        }
        else {
            node->compressed_link = get_compressed_link(suf_link);
        }

        return node->compressed_link;
    }
private:
    Node *root;

    void add_string(const string &str, size_t str_num)
    {
        Node *cur_node = root;
        for(const char sym : str)
        {
            if(cur_node->next[sym] == nullptr)
            {
                Node *new_node = new Node(cur_node, sym);
                cur_node->next[sym] = new_node;
            }
            cur_node = cur_node->next[sym];
        }

        cur_node->str_num.push_back(str_num);
    }
};

void temp_split(const string &str, vector<string> &substrs,
                          vector<size_t> &sub_poss)
{
    int last_question_pos = -1;
    for(size_t i = 0; i < str.size(); ++i)
    {
        if(str[i] == '?')
        {
            if(i - last_question_pos - 1 != 0)
            {
                sub_poss.push_back(i);
                substrs.push_back(str.substr(last_question_pos + 1,
                                         i - last_question_pos - 1));
            }
            last_question_pos = i;
        }
    }
    if(str.size() - last_question_pos - 1 != 0)
    {
        sub_poss.push_back(str.size());
        substrs.push_back(str.substr(last_question_pos + 1,
                                    str.size() - last_question_pos - 1));
    }
}

vector<size_t> find_temps(const string &text, const string &pattern)
{
    vector<size_t> temp_poss;
    if(text.size() < pattern.size()) {
        return temp_poss;
    }

    vector<string> substrs;
    vector<size_t> substr_poss;
    temp_split(pattern, substrs, substr_poss);
    Trie trie(substrs);

    Node *cur_node = trie.get_root();
    vector<uint16_t> substrs_num(text.size());
    for(size_t i = 0; i < text.size(); ++i)
    {
        char cur_sym = text[i];
        cur_node = trie.get_next_node(cur_node, cur_sym);

        Node *node = cur_node;
        if(node->str_num.empty()) {
            node = trie.get_compressed_link(node);
        }
        while(node != trie.get_root())
        {
            for(int str_num : node->str_num)
            {
                if(i + 1 >= substr_poss[str_num])
                {
                    size_t sub_pos = i + 1 - substr_poss[str_num];
                    ++substrs_num[sub_pos];
                }
            }
            node = trie.get_compressed_link(node);
        }
    }

    for(size_t i = 0; i < text.size(); ++i)
    {
        if(substrs_num[i] == substrs.size() && i + pattern.size() <= text.size()) {
            temp_poss.push_back(i);
        }
    }

    return temp_poss;
}

int main()
{
    string pattern, text;
    cin >> pattern >> text;

    vector<size_t> temp_poss = find_temps(text, pattern);
    for(size_t i : temp_poss) {
        cout << i << " ";
    }

    return 0;
}
